package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"unicode"
)

func checkDuplicates(a string, b string) ([]byte, error) {
    var duplicateBytes []byte
    for i := 0; i < len(a); i++ {
        for j := 0; j < len(b); j++ {
            if a[i] == b[j] {
                duplicateBytes = append(duplicateBytes, a[i])
            }
        }
    }

    if len(duplicateBytes) == 0 {
        return []byte{0}, errors.New("Unable to find a duplicate")
    }
    return duplicateBytes, nil
}

func threeWayCheckDuplicate(a, b, c string) ([]byte, error) {
    dup1, err := checkDuplicates(a, b)
    if err != nil {
        panic(err)
    }
    dup2, err := checkDuplicates(c, string(dup1[:]))
    if err != nil {
        panic(err)
    }
    return dup2, nil
}

func main() {
    // Part 1
    fi, err := os.Open("input.txt") 
    if err != nil {
        panic (err)
    }
    defer fi.Close()
    scanner := bufio.NewScanner(fi)
    total := 0
    for scanner.Scan() {
        // Continue on blank lines
        line := scanner.Text()
        if line == "" {
            continue
        }
        linePart1 := line[len(line)/2:]
        linePart2 := line[:len(line)/2]
        dup, err := checkDuplicates(linePart1, linePart2)
        if err != nil {
            panic(err)
        }

        value := int(unicode.ToLower(rune(dup[0])))
        if unicode.IsUpper(rune(dup[0])) {
            value += 26
        }
        total += value - 96
    }

    // Part 2
    fi.Seek(0, io.SeekStart)
    scanner = bufio.NewScanner(fi)
    part2Total := 0
    duplicates := []byte{}
    rucksacks := []string{}
    elfCount := 0
    for scanner.Scan() { 
        line := scanner.Text()
        // if elfCount mod 3 = 0, we find the duplicate and start a new group
        if elfCount % 3 == 0 && elfCount != 0 {
            duplicates, err = threeWayCheckDuplicate(rucksacks[0], rucksacks[1], rucksacks[2])
            if err != nil {
                panic(err)
            }
            value := int(unicode.ToLower(rune(duplicates[0])))
            if unicode.IsUpper(rune(duplicates[0])) {
                value += 26
            }
            part2Total += value - 96

            // start new group
            rucksacks = []string{}
        }

        rucksacks = append(rucksacks, line)
        elfCount += 1
    }

    fmt.Printf("The total for part one is: %d\n", total)
    fmt.Printf("The total for part two is: %d\n", part2Total)

}
