package main

import (
	"bufio"
    "fmt"
	"io"
	"log"
	"os"
)

func countUniqueElems(s []byte) int {
    unique := make(map[byte]bool, len(s))
    for _, elem := range s {
        unique[elem] = true
    }
    return len(unique)
}

func main() {
    LEN_START_PACKET := 4    // start-of-packet marker is first 4 unique characters
    LEN_START_MESSAGE := 14  // start-of-message marker is first 14 unique characters

    inFile, err := os.Open("input.txt")
    if err != nil {
        panic(err)
    }
    defer inFile.Close()

    reader := bufio.NewReader(inFile)

    var startOfPacketQueue []byte
    var startOfMessageQueue []byte

    startPacket := 0
    startMessage := 0
    currentChar := 0
    for {
        // No need to continue if we've found the start-of-packet and start-of-message
        if startMessage != 0 && startPacket != 0 {
            break
        }

        currentChar += 1

        c, err := reader.ReadByte()
        if err != nil {
            if err == io.EOF {
                break
            } else {
                log.Fatal(err)
            }
        }

        // Only check packet if we haven't found the first start packet yet
        if startPacket == 0 {
            startOfPacketQueue = append(startOfPacketQueue, c)
            // If statement is necessary to allow slice to grow until it reaches the desired length
            if len(startOfPacketQueue) > LEN_START_PACKET {
                startOfPacketQueue = startOfPacketQueue[1:]
            }
            uniquePacket := countUniqueElems(startOfPacketQueue)
            if uniquePacket == LEN_START_PACKET {
                startPacket = currentChar
            }
        }

        // Only check message if we haven't found the first start message yet
        if startMessage == 0 {
            startOfMessageQueue = append(startOfMessageQueue, c)
            // If statement is necessary to allow slice to grow until it reaches the desired length
            if len(startOfMessageQueue) > LEN_START_MESSAGE {
                startOfMessageQueue = startOfMessageQueue[1:]
            }
            uniqueMessage := countUniqueElems(startOfMessageQueue)
            if uniqueMessage == LEN_START_MESSAGE {
                startMessage = currentChar
            }
        }
    }

    fmt.Printf("Part 1: First start-of-packet marker found at character %d\n", startPacket)
    fmt.Printf("Part 2: First start-of-message marker found at character %d\n", startMessage)
}

