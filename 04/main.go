package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

func contains(a, b []int) bool {
    // Sort arrays, then compare first and last elements of each array
    sort.Ints(a)
    sort.Ints(b)

    aFirst := a[0]
    aLast := a[len(a)-1]
    bFirst := b[0]
    bLast := b[len(b)-1]

    // If either array is length 1
    if len(a) == 1 {
        if bFirst <= aFirst && aFirst <= bLast {
            return true
        }
        return false
    }
    if len(b) == 1 {
        if aFirst <= bFirst && bFirst <= aLast {
            return true
        }
        return false
    }

    // 6 total cases
    if aFirst == bFirst {
        if bFirst <= aFirst && aFirst <= bLast {
            return true
        } else if aFirst <= bFirst && bFirst <= aLast {
            return true
        } else {
            return false
        }
    } else if aFirst < bFirst {
        if aLast >= bLast {
            return true
        }
        return false
    } else {
        if aLast <= bLast {
            return true
        }
        return false
    }
}

func overlaps(a, b []int) bool {
    sort.Ints(a)
    sort.Ints(b)

    aFirst := a[0]
    aLast := a[len(a)-1]
    bFirst := b[0]
    bLast := b[len(b)-1]

    if aFirst <= bFirst && aLast >= bFirst {
        return true
    } else if aFirst >= bFirst && aFirst <= bLast {
        return true
    } else {
        return false
    }
}

func createIntArray(s string) []int {
    endpoints := strings.Split(s, "-")
    var intEndpoints []int
    for _, endpoint := range endpoints {
        intV, err := strconv.Atoi(endpoint)
        if err != nil {
            panic(err)
        }
        intEndpoints = append(intEndpoints, intV)
    }
    var newArray []int
    for i := intEndpoints[0]; i <= intEndpoints[1]; i++ {
        newArray = append(newArray, i)
    }
    return newArray
}

func main() {
    inFile, err := os.Open("input.txt")
    if err != nil {
        panic(err)
    }
    defer inFile.Close()

    // Part 1
    numContains := 0
    scanner := bufio.NewScanner(inFile)
    for scanner.Scan() {
        line := scanner.Text()
        if line == "" {
            continue
        }
        sections := strings.Split(line, ",")
        if len(sections) != 2 {
            panic("Section split failed")
        }
        sectionOneIntArray := createIntArray(sections[0])
        sectionTwoIntArray := createIntArray(sections[1])

        if contains(sectionOneIntArray, sectionTwoIntArray) {
            numContains += 1
        }
    }

    // Part 2
    inFile.Seek(0, io.SeekStart)
    scanner = bufio.NewScanner(inFile)
    numOverlaps := 0
    for scanner.Scan() {
        line := scanner.Text()
        if line == "" {
            continue
        }
        sections := strings.Split(line, ",")
        if len(sections) != 2 {
            panic("Section part 2 split failed")
        }
        sectionOneIntArray := createIntArray(sections[0])
        sectionTwoIntArray := createIntArray(sections[1])

        if overlaps(sectionOneIntArray, sectionTwoIntArray) {
            numOverlaps += 1
        }
    }
    fmt.Printf("Number of found contains in all pairs: %d\n", numContains)
    fmt.Printf("Number of found overlaps in all pairs: %d\n", numOverlaps)
}
