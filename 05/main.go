package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Format of s is "move numCratesToMove from startStackIdx to destStackIdx"
func getTokens(s string) (int, int, int) {
    tokens := strings.Split(s, " ")
    numCratesToMove, err:= strconv.Atoi(tokens[1])
    if err != nil {
        panic(err)
    }
    startStackIdx, err := strconv.Atoi(tokens[3])
    if err != nil {
        panic(err)
    }
    startStackIdx -= 1
    destStackIdx, err := strconv.Atoi(tokens[5])
    if err != nil {
        panic(err)
    }
    destStackIdx -= 1

    return numCratesToMove, startStackIdx, destStackIdx
}

func main() {
    // Initial state of all the stacks.
    stack1 := []byte{'D', 'H', 'R', 'Z', 'S', 'P', 'W', 'Q',}
    stack2 := []byte{'F', 'H', 'Q', 'W', 'R', 'B', 'V',}
    stack3 := []byte{'H', 'S', 'V', 'C',}
    stack4 := []byte{'G', 'F', 'H',}
    stack5 := []byte{'Z', 'B', 'J', 'G', 'P',}
    stack6 := []byte{'L', 'F', 'W', 'H', 'J', 'T', 'Q',}
    stack7 := []byte{'N', 'J', 'V', 'L', 'D', 'W', 'T', 'Z',}
    stack8 := []byte{'F', 'H', 'G', 'J', 'C', 'Z', 'T', 'D',}
    stack9 := []byte{'H', 'B', 'M', 'V', 'P', 'W',}
    stacks := []*[]byte{
        &stack1, &stack2, &stack3, &stack4, &stack5, &stack6, &stack7, &stack8, &stack9,
    }
    //stack1 := []byte{'N', 'Z',}
    //stack2 := []byte{'D', 'C', 'M',}
    //stack3 := []byte{'P',}
    //stacks := []*[]byte{
    //    &stack1, &stack2, &stack3,
    //}

    inFile, err := os.Open("input.txt")
    if err != nil {
        panic(err)
    }

    // Part 1
    //scanner := bufio.NewScanner(inFile)
    //for scanner.Scan() {
    //    line := scanner.Text()
    //    if len(line) == 0 || line[0:4] != "move" {
    //        continue
    //    }

    //    numCratesToMove, startStackIdx, destStackIdx := getTokens(line)

    //    for i := 0; i < numCratesToMove; i++ {
    //        //// These lines pops the last crate off the stack
    //        //startStack := *stacks[startStackIdx]
    //        //crate := startStack[len(startStack) - 1]  
    //        //*stacks[startStackIdx] = startStack[: len(startStack) - 1]
    //        //
    //        //// These lines append the crate to the desk stack
    //        //destStack := *stacks[destStackIdx]
    //        //*stacks[destStackIdx] = append(destStack, crate)

    //        // These lines pop the crate off the first element of the stack
    //        startStack := *stacks[startStackIdx]
    //        crate := startStack[0]
    //        *stacks[startStackIdx] = startStack[1:]

    //        // These lines push the crate tho the beginning of the stack
    //        destStack := *stacks[destStackIdx]
    //        *stacks[destStackIdx] = append([]byte{crate}, destStack...)
    //    } 
    //}

    //var partOneString string
    //for i := 0; i < len(stacks); i++ {
    //    stack := *stacks[i]
    //    crate := stack[0]
    //    partOneString += string(crate)
    //}
    //fmt.Printf("Part one result is: %s\n", partOneString)


    // Part 2
    inFile.Seek(0, io.SeekStart)
    scanner := bufio.NewScanner(inFile)
    for scanner.Scan() {
        line := scanner.Text()
        if len(line) == 0 || line[0:4] != "move" {
            continue
        }

        numCratesToMove, startStackIdx, destStackIdx := getTokens(line)

        // Pop crates off 
        startStack := *stacks[startStackIdx]
        crates := make([]byte, numCratesToMove)
        copy(crates, startStack[:numCratesToMove])
        *stacks[startStackIdx] = startStack[numCratesToMove:]

        // Push crates on
        destStack := *stacks[destStackIdx]
        *stacks[destStackIdx] = append(crates, destStack...)
    }

    fmt.Println()
    var partTwoString string
    for i := 0; i < len(stacks); i++ {
        stack := *stacks[i]
        fmt.Printf("%s\n", stack)
        crate := stack[0]
        partTwoString += string(crate)
    }
    fmt.Printf("Part two result is: %s\n", partTwoString)
}
