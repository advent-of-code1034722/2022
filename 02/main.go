package main

import (
	"bufio"
    "fmt"
	"os"
	"strings"
)

type Result string
const (
    ResultWin Result = "win"
    ResultDraw Result = "draw"
    ResultLoss Result = "loss"
)

type Play string
const (
    ROCK Play = "rock"
    PAPER Play = "paper"
    SCISSORS Play = "scissors"
)

// winLossDraw determines if the result of a rock paper scissors hand is a win,
// loss, or draw for the `you' argument. A call of winLossDraw("A", "B") returns
// a ResultWin since paper ("B") beats rock ("A").
func winLossDraw(opponent Play, you Play) Result {
    // Same result is a draw, 3 points
    if opponent == you {
        return ResultDraw
    }

    // Loss, 0 points
    if opponent == ROCK && you == SCISSORS {
        return ResultLoss
    }
    if opponent == SCISSORS && you == PAPER {
        return ResultLoss
    }
    if opponent == PAPER && you == ROCK {
        return ResultLoss
    }

    // Win, 6 points
    if opponent == ROCK && you == PAPER {
        return ResultWin
    }
    if opponent == PAPER && you == SCISSORS {
        return ResultWin
    }
    if opponent == SCISSORS && you == ROCK {
        return ResultWin
    }

    panic("Unable to determine winner")
}


// determinePlay returns the Play value required to achieve result based on
// the opponent's play. For example, if the opponent plays ROCK and the desired
// result is a draw, determinePlay(ROCK, ResultDraw) returns ROCK.
func determinePlay(opponent Play, result Result) Play {
    // Same result is a draw, 3 points
    if result == ResultDraw {
        return opponent
    }

    // Loss, 0 points
    if result == ResultLoss {
        switch {
            case opponent == ROCK: return SCISSORS
            case opponent == PAPER: return ROCK
            case opponent == SCISSORS: return PAPER
        }
    }

    // Win, 6 points
    if result == ResultWin {
        switch {
            case opponent == ROCK: return PAPER
            case opponent == PAPER: return SCISSORS
            case opponent == SCISSORS: return ROCK
        }
    }

    panic("Unable to determine play")
}

// points returns the total points received from a game of rock paper scissors based
// on the following rules.
//
//  - Losses are 0 points
//  - Draws are 3 points
//  - Wins are 6 points
//  - If you played Rock, you get an additional 1 point, regardless of result
//  - If you played Paper, you get an additional 2 points, regardless of result
//  - If you played Scissors, you get an additional 3 points, regardless of result
func points(play Play, result Result) int {
    score := 0
    switch {
        case result == ResultLoss: score += 0
        case result == ResultDraw: score += 3
        case result == ResultWin: score += 6
    }

    switch {
        case play == ROCK: score += 1
        case play == PAPER: score += 2
        case play == SCISSORS: score += 3
    }

    return score
}


func main() {
    fi, err := os.Open("input.txt") 
    if err != nil {
        panic(err)
    }
    defer fi.Close()

    scanner := bufio.NewScanner(fi)
    partOneScore := 0
    partTwoScore := 0
    for scanner.Scan() {
        // Continue if it's a blank line
        if scanner.Text() == "" {
            continue
        }

        vals := strings.Split(scanner.Text(), " ")
        var opponent, you Play

        // Part 1, X => ROCK, Y => PAPER, Z => SCISSORS
        switch {
            case vals[0] == "A": opponent = ROCK
            case vals[0] == "B": opponent = PAPER
            case vals[0] == "C": opponent = SCISSORS
        }

        switch {
            case vals[1] == "X": you = ROCK
            case vals[1] == "Y": you = PAPER
            case vals[1] == "Z": you = SCISSORS
        }
        result := winLossDraw(opponent, you)
        partOneScore += points(you, result)

        // Part 2, X => ResultLoss, Y => ResultDraw, Z => ResultWin
        // duplicated logic but I left it in for readability since this is advent of code
        switch { 
            case vals[0] == "A": opponent = ROCK
            case vals[0] == "B": opponent = PAPER
            case vals[0] == "C": opponent = SCISSORS
        }
        switch {
            case vals[1] == "X": result = ResultLoss
            case vals[1] == "Y": result = ResultDraw
            case vals[1] == "Z": result = ResultWin
        }
        play := determinePlay(opponent, result)
        partTwoScore += points(play, result)
    }

    fmt.Printf("Score for part one is: %d\n", partOneScore)
    fmt.Printf("Score for part two is: %d\n", partTwoScore)
}
