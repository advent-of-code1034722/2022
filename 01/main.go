package main

import (
	"bufio"
	"fmt"
	"os"
    "sort"
	"strconv"
)

func main() {

    fi, err := os.Open("input.txt")
    if err != nil {
        panic(err)
    }
    defer fi.Close()

    scanner := bufio.NewScanner(fi)
    currentCount := 0
    var counts []int
    for scanner.Scan() {
        num, err := strconv.Atoi(scanner.Text())
        // If err != nil, we've hit a blank line
        if err != nil {
            counts = append(counts, currentCount)
            currentCount = 0
            continue
        }
        currentCount += num
    }

    sort.Ints(counts)

    fmt.Printf("The elf with the most calories is carrying %d calories\n", counts[len(counts)-1])
    fmt.Printf("The top three elves are carrying %d in total\n", counts[len(counts)-1] + counts[len(counts)-2] + counts[len(counts)-3])

}
