use std::fs::File;
use std::io::{Read, BufRead, BufReader};


fn main() -> std::io::Result<()> {
    let inFile = File::open("../input.txt")?;
    let reader = BufReader::new(inFile);

    let mut counts: Vec<i32> = Vec::new();
    let mut currentCount = 0;

    for (index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if line.is_empty() {
            counts.push(currentCount);
            currentCount = 0;
            continue;
        }

        let value: i32 = line.parse().unwrap();
        currentCount += value;
    }

    counts.sort();
    if let Some(val) = counts.last() {
        println!("The total for part 1 is: {}", val);
    }

    let sum: i32 = counts.iter().rev().take(3).sum();
    println!("The total for part 2 is: {}", sum);

    Ok(())
}
